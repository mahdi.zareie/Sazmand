from django.core.management import BaseCommand
from django.db import IntegrityError
from django.utils.text import slugify

from rss_parser import models
from urllib import parse


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--opml', action='store', dest='opml',
            help='opml file to read',
        )

    def handle(self, **options):
        import listparser
        obj = listparser.parse(options['opml'])
        fc, rc = 0, 0
        for feed in obj['feeds']:
            try:
                url = feed['url']
                obj, created = models.Feed.objects.get_or_create(name=parse.urlparse(url).netloc)
                if created:
                    fc += 1
                models.RSSFeed.objects.create(
                    rss_name=slugify(feed['title']),
                    feed=obj,
                    url=url,
                )
                rc += 1
            except IntegrityError:
                print("IE: {}".format(feed))
        print("New feeds: {}, new RSS: {}".format(fc, rc))
