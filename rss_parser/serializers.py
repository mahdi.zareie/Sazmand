from rest_framework import serializers
from . import models


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Feed
        fields = ("name", )


class RSSFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RSSFeed
        exclude = ("last_update", )


class RSSEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RSSEntry
        fields = serializers.ALL_FIELDS
