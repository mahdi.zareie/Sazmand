from django.urls import path
from . import views

urlpatterns = [
    path('feeds', views.FeedsList.as_view()),
    path('rss-urls', views.RssList.as_view()),
    path('entries', views.RssEntry.as_view()),
]
