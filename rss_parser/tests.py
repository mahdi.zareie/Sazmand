import json

from rest_framework.test import APITestCase


class APITests(APITestCase):
    fixtures = [
        'feeds',
        'rss',
        'entries'
    ]

    def test_get_all_feeds(self):
        response = self.client.get("/api/v1/contents/feeds")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 2)

    def test_get_all_rss(self):
        response = self.client.get("/api/v1/contents/rss-urls")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 2)

    def test_get_rss_filtered_by_feed(self):
        response = self.client.get("/api/v1/contents/rss-urls?feed=lobste.rs")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 1)

    def test_get_all_entries(self):
        response = self.client.get("/api/v1/contents/entries?page=1")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 25)

        response = self.client.get("/api/v1/contents/entries?page=2")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 25)

        response = self.client.get("/api/v1/contents/entries?page=3")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content.decode("utf8"))
        self.assertEqual(len(data['results']), 5)




