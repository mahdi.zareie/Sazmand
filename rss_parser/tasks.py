import traceback
from datetime import datetime

from django.db import IntegrityError
from rss_parser.models import RSSFeed, RSSEntry
import logging
from concurrent.futures.thread import ThreadPoolExecutor
from rss_parser.parsers import FeedParser

rss_logger = logging.getLogger()  # TODO setup logging
parser = FeedParser()


def load_all_rss():
    with ThreadPoolExecutor(max_workers=4) as pool:
        for rss in RSSFeed.objects.filter(enabled=True).order_by('last_update'):
            rss_logger.error("{} from {}".format(rss.rss_name, rss.feed_id))
            pool.submit(_load_rss, rss)


def _load_rss(rss: RSSFeed):
    try:
        for entry in parser.parse_feed(rss.url):
            try:
                RSSEntry.objects.create(
                    feed_id=rss.feed_id,
                    rss=rss,
                    url=entry['url'],
                    published_date=entry['published'],
                    title=entry['title'],
                )
                rss_logger.debug("New entry stored: {}".format(entry['url']))
            except IntegrityError as exp:
                rss_logger.warning("Duplicate url ignored: {}".format(entry['url']))
            except Exception as exp:
                rss_logger.error("Unexpected error while reading rss: {}".format(exp))
        rss.last_update = datetime.now()
        rss.save()
    except Exception as exp:
        rss_logger.error(exp, traceback.format_exc())
