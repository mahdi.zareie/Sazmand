from django.db import models
from django.conf import settings


class Feed(models.Model):
    class Meta:
        unique_together = [("name", "owner"), ]
    name = models.CharField(max_length=32, db_index=True)
    owner = models.ForeignKey(getattr(settings, "AUTH_USER_MODE"), on_delete=models.Case, related_name="feeds")


class RSSFeed(models.Model):
    rss_name = models.CharField(max_length=32, primary_key=True)
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)
    url = models.CharField(max_length=128)
    enabled = models.BooleanField(default=True)
    last_update = models.DateTimeField(null=True)


class RSSEntry(models.Model):
    class Meta:
        unique_together = [("feed", "url")]
    feed = models.ForeignKey(Feed, null=True, on_delete=models.SET_NULL)
    rss = models.ForeignKey(RSSFeed, null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=140)
    url = models.CharField(max_length=140)
    published_date = models.DateTimeField()
