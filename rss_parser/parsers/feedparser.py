from datetime import datetime
import feedparser
import pytz

from .exceptions import RssContentParsingError


class FeedParser:
    datetime_format = "%a, %d %b %Y %H:%M:%S %z"

    def parse_date(self, date_value):
        try:
            value = datetime.strptime(date_value, self.datetime_format)
            return value.astimezone(pytz.timezone("UTC")).replace(tzinfo=None)
        except Exception as exp:
            print("Error parsing: {}".format(date_value))
            raise


    def parse_feed(self, rss_url):
        result = feedparser.parse(rss_url)
        if 'entries' in result:
            return [
                {
                    **entry,
                    "url": entry['link'],
                    "published": self.parse_date(entry['published'])
                } for entry in result['entries']
            ]
        raise RssContentParsingError("'entries' was not exists in the resulting dict")
