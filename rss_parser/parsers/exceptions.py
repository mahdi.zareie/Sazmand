class RSSParsingError(Exception):
    pass


class HTTPError(RSSParsingError):
    pass


class RssContentParsingError(RSSParsingError):
    pass
