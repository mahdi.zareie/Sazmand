from django.contrib import admin
from . import models


admin.site.register(models.Feed)
admin.site.register(models.RSSFeed)