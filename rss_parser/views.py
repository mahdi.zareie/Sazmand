from rest_framework.generics import ListAPIView
from rest_framework import pagination
from . import models
from . import serializers



class FeedsList(ListAPIView):
    serializer_class = serializers.FeedSerializer

    def get_queryset(self):
        return models.Feed.objects.all()


class RssList(ListAPIView):
    serializer_class = serializers.RSSFeedSerializer

    def get_queryset(self):
        query = models.RSSFeed.objects.filter(enabled=True).all()
        feed = self.request.GET.get("feed", None)
        if feed is not None:
            query = query.filter(feed_id=feed)
        return query


class RssEntry(ListAPIView):
    serializer_class = serializers.RSSEntrySerializer


    def get_queryset(self):
        query = models.RSSEntry.objects.all()
        feed = self.request.GET.get("feed", None)
        if feed is not None:
            query = query.filter(feed_id=feed)
        rss = self.request.GET.get("rss", None)
        if rss is not None:
            query = query.filter(rss_id=rss)
        return query
