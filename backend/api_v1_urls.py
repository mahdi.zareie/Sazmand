from django.urls import path, include

urlpatterns = [
    path('contents/', include('rss_parser.urls'))
]
