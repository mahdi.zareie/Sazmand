from easy_scheduler.simple_scheduler import SimpleThreadScheduler
from rss_parser.tasks import load_all_rss

rss_parser = SimpleThreadScheduler(300, load_all_rss)
rss_parser.run()
